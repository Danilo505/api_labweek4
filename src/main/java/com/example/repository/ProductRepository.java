package com.example.repository;

import com.example.models.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ProductRepository extends JpaRepository<Products, Integer> {

    Optional<List<Products>> findByName (String name);
    Optional<List<Products>> findByPrice (double price);
    Optional<List<Products>> findByQuantity (int quantity);
}
